import React from 'react';
import {Link, useHistory} from 'react-router-dom';
import {useDispatch} from 'react-redux';

import {getSongToEdit} from '../actions/songsActions';

const Song = ({song}) => {

    const history = useHistory();
    const dispatch = useDispatch();

    //funcion para la redireccion programatica
    const redireccionEdicion = song => {
        dispatch(getSongToEdit(song))
        history.push(`/songs/update/${song.name}`)
    }




    return (  
        <tr>
            <td>
                {song.name}
            </td>
            <td>
                {song.author}
            </td>
            <td className="acciones">
                <button onClick={() => {redireccionEdicion(song)}} type="button" className="btn btn-primary mr-2">
                    Editar
                </button>
                <button type="button"
                        className="btn btn-danger">
                            Eliminar
                </button>    
            </td>
        </tr>

    );
}
 
export default Song;