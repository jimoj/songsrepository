import React from 'react'

const UpdateSong = () => {
    return (  
        <div className='row justify-content-center'>
        <div className="col-md-8">
            <div className="card"> 
                <div className="card-body">
                    <h2 className="text-center mb-4 font-weigh-bold"> 
                        Update Song
                    </h2>
                    <form>
                        <div>
                            <label>
                                Nombre de la canción
                            </label>
                            <input 
                                type="text"
                                className="form-control"
                                placeHolder="Nombre de la canción"
                                name="nombre"
                            />

                            <label>Autor de la canción</label>
                            <input 
                                type="text"
                                className="form-control"
                                placeHolder="Autor de la canción"
                                name="autor"
                            />
                        </div>
                        <button
                            type="submit"
                            className="btn btn-primary font-weight-bold text-uppercase d-block w-100">

                        Editar Canción        
                        </button>        
                    </form>

                </div>
            </div>
        </div>
    </div>
    );
}
 
export default UpdateSong;