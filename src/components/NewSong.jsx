import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

//actions de redux
import {crearNuevaCancionAction} from '../actions/songsActions';

const NewSong = () => {

    //state del componente
    const [nombre, guardarNombre] = useState('');
    const [autor, guardarAutor] = useState('');


    // useDispatch manda ejecutar las actiones, esta devuelve o crea otra funcion.
    // Se utiliza para capturar y usar los action 
    const dispatch = useDispatch();
    // Asi es como se llama al action
    const nuevaCancion =  (song) => dispatch(crearNuevaCancionAction(song))



    //Acceso al state del store
    const cargando = useSelector( state => state.songs.loading)
    const error = useSelector (state => state.songs.error)

    const submitNuevaCancion = e => {
        e.preventDefault()

        //validar form
        if(nombre.trim() === '' || autor.trim() === '') {
            return ;
        }


        //si no hay errores

        // crear la nueva cancion
        nuevaCancion({
            nombre,
            autor
        });
    }






    return (  
        <div className='row justify-content-center'>
            <div className="col-md-8">
                <div className="card"> 
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weigh-bold"> 
                            Add new song
                        </h2>
                        <form
                            onSubmit={submitNuevaCancion}
                        >
                            <div>
                                <label>
                                    Nombre de la canción
                                </label>
                                <input 
                                    type="text"
                                    className="form-control"
                                    placeholder="Nombre de la canción"
                                    name="nombre"
                                    value={nombre}
                                    onChange={e => guardarNombre(e.target.value)}
                                />

                                <label>Autor de la canción</label>
                                <input 
                                    type="text"
                                    className="form-control"
                                    placeholder="Autor de la canción"
                                    name="autor"
                                    value={autor}
                                    onChange={e => guardarAutor(e.target.value)}
                                />
                            </div>
                            <button
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase d-block w-100">

                            Agregar Canción        
                            </button>        
                        </form>
                        {cargando ? <p>Cargando...</p> : null}
                        {error ? <p className=" alert alert-danger p2 text-center">Se produjo un error</p> : null}
                    </div>
                </div>
            </div>
        </div>
    );
}
 
export default NewSong;
