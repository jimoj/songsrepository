import React, {Fragment, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux';
import Song from './Song'

//actions de redux
import {getSongsAction} from '../actions/songsActions';


const Songs = () => {


    const dispatch = useDispatch();
    useEffect(() => {
        //Consultar el api
        const cargarCanciones = () => dispatch(getSongsAction());
        cargarCanciones();
        
    }, []);


    const songs = useSelector(state => state.songs.songs)

    return (  
        <Fragment>
            <h2 className="text-center my-5">Listado de Canciones</h2>

            <table className="table table-striped">
                <thead className="bg-primary table-dark">
                    <tr>
                        <th>
                            Nombre
                        </th>
                        <th>
                            Autor
                        </th>
                        <th>
                            Acciones
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {songs.length === 0 ? 'No existen canciones' : songs.map( song => 
                        (
                            <Song key = {song.name}
                                  song = {song}
                                
                            />
                        )
                    )}
                </tbody>
            </table>
        </Fragment>
        
    );
}
 
export default Songs;
