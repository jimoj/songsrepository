import {combineReducers} from 'redux';
import songsReducer from './songsReducer';


export default combineReducers({
    songs: songsReducer //songs en este caso sera el nombre del state
})
