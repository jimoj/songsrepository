import {
    ADD_SONG,
    ADD_SONG_KO,
    ADD_SONG_OK,
    DOWNLOAD_SONGS_KO,
    DOWNLOAD_SONGS_OK,
    STARTING_DOWNLOAD_SONGS,
    STARTING_EDIT_SONGS

} from '../types';


// Cada reducer tiene su propio state
const initialState = {
    songs: [],
    error: null,
    loading: false,
    songEdit: null
}

export default function(state = initialState, action) {
    switch(action.type) {
        case STARTING_DOWNLOAD_SONGS:
        case ADD_SONG: 
            return {
                ...state, // esto es una copia del objeto state (... = copia)
                loading: action.payload
            }
        case ADD_SONG_OK:
            return {
                ...state,
                loading: false,
                songs: [...state.songs, action.payload]
            }
        case DOWNLOAD_SONGS_KO:    
        case ADD_SONG_KO:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case DOWNLOAD_SONGS_OK:
            return {
                ...state,
                loading: false,
                error: null,
                songs : action.payload
            }   
        case STARTING_EDIT_SONGS:
            return {
                ...state,
                songEdit: action.payload
            }        
        default:
            return state;
    }
}