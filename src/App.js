import React, {Fragment} from 'react';
import Header from './components/Header';
import Songs from './components/Songs';
import NewSong from './components/NewSong';
import UpdateSong from './components/UpdateSong';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

//REDUX
import {Provider} from 'react-redux';
import store from './store';

function App() {
  return (
    <Router>
        <Provider store={store}>
          <Header />
          <div className="container">
              <Switch>
                <Route exact path="/" component={Songs} />
                <Route exact path="/songs/new" component={NewSong} />
                <Route exact path="/songs/update/:id" component={UpdateSong} />
              </Switch>
          </div>
        </Provider>
    </Router>
    
  );
}

export default App;
