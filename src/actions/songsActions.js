import {
    ADD_SONG,
    ADD_SONG_KO,
    ADD_SONG_OK,
    DOWNLOAD_SONGS_KO,
    DOWNLOAD_SONGS_OK,
    STARTING_DOWNLOAD_SONGS,
    STARTING_EDIT_SONGS,
    EDIT_SONGS_KO,
    EDIT_SONGS_OK

} from '../types';

import clienteAxios from '../axios';
import Swal from 'sweetalert2';




// Crear nuevas canciones

export function crearNuevaCancionAction(song) {
    return async (dispatch) => {
        dispatch(agregarCancion());

        try {
            
            // insertar en la base de datos
            await clienteAxios.post('/songs/addSong', song);
            dispatch(agregarCancionExito(song));

            Swal.fire(
                'Correcto',
                'La canción se ha agregado correctamente',
                'succes'
            )

        } catch(error) {
            console.log("se ha producido un error")
            dispatch(agregarCancionError(true));
        }
    }
}


// Siempre que se define una funcion en el action habrá que hacerlo en el reducer
const agregarCancion = () => ({
    type: ADD_SONG,
    payload: true
    // payload: parte que modificara los datos del state
})

//Si la cancion se guarda en la base de datos correctamente
const agregarCancionExito = song => ({
    type: ADD_SONG_OK,
    payload: song //se pasa ya que esta vez se modifica el state
})

//Si la cancion no se guarda en la base de datos correctamente
const agregarCancionError = error => ({
    type: ADD_SONG_KO,
    payload: error
})


//Funcion para descargar los canciones de la base de datos
export function getSongsAction () {
    return async (dispatch) => {
        dispatch(downloadSongs());

        try {
            const response = await clienteAxios.get('/songs/findAll')
            dispatch(downloadSongsOk(response))
        } catch (error) {
            console.log(error)
            dispatch(downloadSongsKo())
        }
    }
}

const downloadSongs = () => ({
    type: STARTING_DOWNLOAD_SONGS,
    payload:true
})

const downloadSongsOk = (songs) => ({
    type: DOWNLOAD_SONGS_OK,
    payload:songs
})

const downloadSongsKo = (songs) => ({
    type: DOWNLOAD_SONGS_KO,
    payload:true
})



//Obtener el producto a editar
export function getSongToEdit(song){
    return (dispatch) => {
        dispatch(getSongEditAction(song))
    }
}

const getSongEditAction = song => {
    return {
        type: STARTING_EDIT_SONGS,
        payload: song
    }
}